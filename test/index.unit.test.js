var mocha = require('mocha');
var request = require('supertest');
var rewire = require('rewire');
var should = require('should');
var sinon = require('sinon');

var index = rewire('../src/index');

var describe = mocha.describe;
var it = mocha.it;
var noop = function() {};

describe('Serving up searches', function() {
    function search(cb, searchText = 'a', start = 0, count = 10, sort = 'asc', timeStamp = 1, wipeParams) {
        var searchUrl = wipeParams ? '/search' : '/search?text=' + searchText + '&start=' + start + '&count=' + count + '&sort=' + sort + '&timeStamp=' + timeStamp;
        request(index)
            .get(searchUrl)
            .expect('Content-Type', 'application/json; charset=utf-8')
            .end(function(err, res) {
                cb(err, res);
            });
    }
    var storeSearchModule;
    var searchModuleMock;
    beforeEach(function() {
        searchModuleMock = function(a, b, c, d, cb) {
            cb([{
                id: 1,
                name: "josh"
            }], 0, 200);
        }
        storeSearchModule = index.__get__('searchModule');
        index.__set__('searchModule', function(searchText, pageStart, pageCount, searchSort, cb) {
            searchModuleMock(searchText, pageStart, pageCount, searchSort, cb);
        });
    });
    afterEach(function() {
        index.__set__('searchModule', storeSearchModule);
    });
    it('should return 200 on a normal search',
        function(done) {
            search(function(err, res) {
                if (err) {
                    done(err);
                    return;
                }
                res.status.should.be.exactly(200);
                done();
            });
        }
    );
    it('should correctly substring too long of a search', function(done) {
        var baseSearchText = 'yk';
        var tooLongSearchText = baseSearchText.repeat(150);
        searchModuleMock = function(searchText, b, c, d, cb) {
            searchText.should.be.exactly(baseSearchText.repeat(50));
            cb([{
                id: 1,
                name: "josh"
            }], 0, 200);
            done();
        }
        search(noop, tooLongSearchText);
    });
    it('should return 500 for missing parameters', function(done) {
        search(function(err, res) {
            if (err) {
                done(err);
                return;
            }
            res.status.should.be.exactly(500);
            done();
        }, '', '', '', '', '', true);
    });
    it('should return correctly formed JSON from a normal search', function(done) {
        var results = [{
            id: 1,
            name: 'Daredevil'
        }, {
            id: 2,
            name: 'Danny Rand'
        }];
        var count = 20;
        var status = 300;
        searchModuleMock = function(a, b, c, d, cb) {
            cb(results, count, status);
        };
        search(function(err, res) {
            if (err) {
                done(err);
                return;
            }
            res.should.have.property('status');
            res.status.should.be.exactly(status);
            res.should.have.property('body');
            res.body.should.have.property('results');
            JSON.stringify(res.body.results).should.be.exactly(JSON.stringify(results));
            res.body.should.have.property('count');
            res.body.count.should.be.exactly(count);
            done();
        });
    });
    it('should ignore case on sorting', function(done) {
        searchModuleMock = function(a, b, c, sort, cb) {
            sort.should.be.exactly('asc');
            cb([], 0, 200);
            done();
        }
        search(noop, "Luke Cage", 0, 10, 'ASC');
    });
    it('should ignore case when searching', function(done) {
        var searchText = "Luke Cage";
        searchModuleMock = function(search, b, c, d, cb) {
            search.should.be.exactly(searchText.toLowerCase());
            cb([], 0, 200);
            done();
        }
        search(noop, searchText, 0, 10, 'ASC');
    });
    it('should return the time stamp on a normal search', function(done) {
        var timeStamp = "" + (+new Date());
        search(function(err, res) {
            res.body.should.have.property('timeStamp');
            res.body.timeStamp.should.be.exactly(timeStamp);
            done();
        }, "Iron Fist", 0, 10, 'desc', timeStamp);
    });
    it('should return a timestamp on an errored search', function(done) {
        var timeStamp = "" + (+new Date());
        search(function(err, res) {
            res.body.should.have.property('timeStamp');
            res.body.timeStamp.should.be.exactly(timeStamp);
            done();
        }, "", -1, 0, '', timeStamp);
    });
    /*it('should replace non-alphnumeric and space characters in the search text',function(done){
        var searchText = "   &=?123 Marvel Defenders~!@#$*(%*^(*( 2017   ";
        var strippedText = "123 Marvel Defenders 2017"
        searchModuleMock = function(search,b,c,d,cb){
            console.log(search);
            search.should.be.exactly(strippedText);
            cb([],0,200);
            done();
        }
        search(noop,searchText);
    });*/
    describe('throw an error for common incorrect parameter values', function() {
        var testParams = [{
                text: '',
                description: 'empty search text'
            }, //Empty search text
            {
                start: -1,
                description: 'a negative start'
            }, //Negative start
            {
                start: 'a',
                description: 'a NaN start'
            }, //NaN type start
            {
                count: 0,
                description: 'no items'
            }, //Requesting no items
            {
                count: 'b',
                description: 'NaN items'
            }, //Requesting NaN items
            {
                count: -1,
                description: 'negative items'
            }, //Requesting a negative amount of items
        ];
        testParams.forEach(function(testParams) {
            it('should throw an error for ' + testParams['description'], function(done) {
                var searchText = testParams['text'] !== undefined ? testParams['text'] : 'Jessica Jones';
                var start = testParams['start'] !== undefined ? testParams['start'] : 0;
                var count = testParams['count'] !== undefined ? testParams['count'] : 10;
                search(function(err, res) {
                    if (err) {
                        done(err);
                        return;
                    }
                    res.should.have.property('status');
                    res.status.should.be.exactly(500);
                    done();
                }, searchText, start, count);
            });
        });
    });
});