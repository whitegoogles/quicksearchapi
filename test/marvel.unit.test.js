var mocha = require('mocha');
var request = require('supertest');
var rewire = require('rewire');
var should = require('should');
var sinon = require('sinon');
var NodeCache = require('node-cache');

var marvel = rewire('../src/marvel');

var describe = mocha.describe;
var it = mocha.it;
var noop = function() {};

//Shamelessly taken from here - thanks jfriend00
//http://stackoverflow.com/questions/23481979/function-to-convert-url-hash-parameters-into-object-key-value-pairs
function parseParams(str) {
    var pieces = str.split("&"),
        data = {},
        i, parts;
    // process each query pair
    for (i = 0; i < pieces.length; i++) {
        parts = pieces[i].split("=");
        if (parts.length < 2) {
            parts.push("");
        }
        data[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
    }
    return data;
}

describe('Searching the Marvel database for characters', function() {

    var storeRequest;
    var requestMock;
    var requestsCache;
    beforeEach(function() {
        storeRequest = marvel.__get__('request');
        requestMock = function(url, cb) {};
        marvel.__set__('request', function(url, cb) {
            requestMock(url, cb);
        });
        requestsCache = new NodeCache();
    });
    afterEach(function() {
        marvel.__set__('request', storeRequest);
    });

    describe('caching', function() {
        it('should return a cached request', function(done) {
            requestMock = function(url, cb) {
                cb(false, {
                    statusCode: 200
                }, JSON.stringify({
                    data: {
                        results: [{
                            id: 1,
                            name: 'Madame Gao',
                            thumbnail: {
                                path: 'https://www.gmail.com',
                                extension: 'jpg'
                            },
                            link: 'https://www.google.com/news'
                        }],
                        total: 3
                    }
                }));
            }
            marvel('a', 0, 5, 'asc', function(results, count, status) {
                requestMock = done;
                marvel('a', 0, 5, 'asc', function(newResults, newCount, newStatus) {
                    JSON.stringify(results).should.be.exactly(JSON.stringify(newResults));
                    count.should.be.exactly(newCount);
                    status.should.be.exactly(newStatus);
                }, requestsCache);
                done();
            }, requestsCache);
        });
        it('should return no results if a substring of the search string is cached with empty results', function() {
            requestMock = function(url, cb) {
                cb(false, {
                    statusCode: 200
                }, JSON.stringify({
                    data: {
                        results: [],
                        total: 0
                    }
                }));
            };
            marvel('Xy', 0, 5, 'asc', function(results, count, status) {
                marvel('Xyz', 0, 5, 'asc', function(newResults, newCount, newStatus) {
                    console.log(newResults);
                    console.log(newCount);
                    newCount.should.be.exactly(0);
                    JSON.stringify(newResults).should.be.exactly(JSON.stringify([]));
                }, requestsCache);
            }, requestsCache);
        });
        it('should execute a search if the substrings in the cache don\'t match the search', function(done) {
            requestMock = function(url, cb) {
                cb(false, {
                    statusCode: 200
                }, JSON.stringify({
                    data: {
                        results: [],
                        total: 0
                    }
                }));
            };
            marvel('dard', 0, 5, 'asc', noop, requestsCache);
            marvel('as', 0, 5, 'asc', noop, requestsCache);
            marvel('are', 0, 5, 'asc', noop, requestsCache);
            marvel('de', 0, 5, 'asc', noop, requestsCache);
            requestMock = function() {
                done();
            };
            marvel('dare', 0, 5, 'asc', noop, requestsCache);
        });
    });

    it('should return 500 on no response from the Marvel server', function(done) {
        requestMock = function(url, cb) {
            cb(true, null, null);
        }
        marvel('a', 0, 5, 'asc', function(results, count, status) {
            status.should.be.exactly(500);
            done();
        }, requestsCache);
    });
    it('should return 500 on a non-200 status code from the Marvel server', function(done) {
        requestMock = function(url, cb) {
            cb(null, {
                statusCode: 300
            }, null);
        }
        marvel('a', 0, 5, 'asc', function(results, count, status) {
            status.should.be.exactly(500);
            done();
        }, requestsCache);
    });
    it('should return 500 on any error while parsing the Marvel server response', function(done) {
        requestMock = function(url, cb) {
            cb(null, {
                statusCode: 200
            }, null);
        }
        marvel('a', 0, 5, 'asc', function(results, count, status) {
            status.should.be.exactly(500);
            done();
        }, requestsCache);
    });

    //TODO These two tests are a little repetitive maybe refactor?
    it('should construct an empty set of items based on the Marvel server response', function(done) {
        requestMock = function(url, cb) {
            cb(null, {
                statusCode: 200
            }, JSON.stringify({
                data: {
                    results: [],
                    total: 0
                }
            }));
        }
        marvel('a', 0, 5, 'asc', function(actualResults, actualCount, status) {
            status.should.be.exactly(200);
            JSON.stringify(actualResults).should.be.exactly('[]');
            actualCount.should.be.exactly(0);
            done();
        }, requestsCache);
    });
    it('should construct a correct set of items based on the Marvel server response', function(done) {

        var offset = 12;
        //Not to be confusing, normally we only care about the name coming back from
        //marvel, but this makes it easy to write the object checker by tacking
        //an id on as well
        var results = [{
                thumbnail: {
                    path: 'Ant-man',
                    extension: 'png'
                },
                name: 'Dr. Manhattan',
                id: offset + 1,
                link: "http://www.marvel.com"
            },
            {
                thumbnail: {
                    path: 'Rorschach',
                    extension: 'jpeg'
                },
                name: 'Flash',
                id: offset + 2,
                urls: [{
                    type: 'detail',
                    url: 'https://www.google.com'
                }],
                link: "https://www.google.com"
            }
        ];
        var total = 30;
        requestMock = function(url, cb) {
            cb(null, {
                statusCode: 200
            }, JSON.stringify({
                data: {
                    results: results,
                    total: total
                }
            }));
        }
        marvel('a', offset, 5, 'asc', function(actualResults, actualCount, status) {
            status.should.be.exactly(200);
            var processedResults = [];
            results.forEach(function(character) {
                var processedCharacter = {
                    name: character.name,
                    id: character.id,
                    picture: character.thumbnail.path + '/' + 'standard_medium' + '.' + character.thumbnail.extension,
                    link: character.link
                };
                processedResults.push(processedCharacter);
            });
            JSON.stringify(actualResults).should.be.exactly(JSON.stringify(processedResults));
            actualCount.should.be.exactly(total);
            done();
        }, requestsCache);
    })
    describe('constructing good URLs based on various good params', function() {
        var params = [{
                input: {
                    searchText: 'Hulk',
                    pageStart: 0,
                    pageCount: 10,
                    searchSort: 'asc'
                },
                output: {
                    nameStartsWith: 'Hulk',
                    offset: 0,
                    limit: 10,
                    orderBy: 'name'
                }
            },
            {
                input: {
                    searchText: 'Iron Man',
                    pageStart: 12,
                    pageCount: 15,
                    searchSort: 'desc'
                },
                output: {
                    nameStartsWith: 'Iron Man',
                    offset: 12,
                    limit: 15,
                    orderBy: '-name'
                }
            },
            {
                input: {
                    searchText: 'Superman2',
                    pageStart: 300,
                    pageCount: 40,
                    searchSort: 'asc'
                },
                output: {
                    nameStartsWith: 'Superman2',
                    offset: 300,
                    limit: 40,
                    orderBy: 'name'
                }
            }
        ];
        params.forEach(function(test) {
            it('should create a correct URL', function(done) {
                var output = test.output;
                requestMock = function(url) {
                    var urlParts = url.split('?');
                    var baseUrl = urlParts[0];
                    (baseUrl + '?').should.be.exactly(marvel.__get__('baseUrl'));
                    var urlData = parseParams(urlParts[1]);
                    urlData['ts'].should.be.above(0);
                    urlData['apikey'].should.be.exactly(marvel.__get__('publicKey'));
                    urlData['hash'].should.be.not.empty();
                    urlData['nameStartsWith'].should.be.exactly(output.nameStartsWith);
                    urlData['offset'].should.be.exactly("" + output.offset);
                    urlData['limit'].should.be.exactly("" + output.limit);
                    urlData['orderBy'].should.be.exactly("" + output.orderBy);
                    done();
                }
                var input = test.input;
                marvel(input.searchText, input.pageStart, input.pageCount, input.searchSort, () => {}, requestsCache);
            });
        });
    });
});