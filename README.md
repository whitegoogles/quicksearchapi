For more information on the general Marvel Search project including testing, architecture, and features see [here](https://docs.google.com/document/d/1xyTVftUvKTWCja-7SBCihl_hMfwPayXVS0T1TcM6uzk/edit?usp=sharing)

On first install

1. Run npm install
2. Run npm install -g istanbul 
3. Run node src/index

The server should start on port 3000

## Folder Structure

```
quick-search-api/
  README.md
  node_modules/
  package.json
  keys.json
  src/
    marvel.js
    index.js
  test/
    marvel.unit.test.js
    index.unit.test.js
```

To do a quick test once you've started the server, open [this](http://localhost:3000/search?text=d&start=0&count=10&sort=asc&timeStamp=1) in your browser and you should see a bunch of JSON with marvel character names in it in your browser.

## Available Scripts

In the project directory, you can run:

### `node src/index`

Launches the server at localhost:3000

### `npm test`

Launches the test runner<br>

### `npm run cover`

Runs the tests and creates a coverage report.<br>
Coverage can be viewed by opening coverage/lcov-report/index.html in the project directory. <br>