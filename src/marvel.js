/** 
 * marvel.js searches the Marvel API for character data
 * If I had known about the marvel npm package before I wrote this, I would
 * probably have used that instead of raw http requests
 **/

var fs = require('fs');
var request = require('request');
var md5 = require('md5');

var baseUrl = "http://gateway.marvel.com/v1/public/characters?";

//TODO This reads the Marvel developer keys in so it can interface correctly
//with the Marvel database. Although I removed keys.json from the gitignore
//to make demoing this easy, ideally in production these would not be stored
//in version control.
try {
    var keys = JSON.parse(fs.readFileSync('keys.json').toString());
} catch (err) {
    console.log('Please place your Marvel public and private keys in the root directory in a file called keys.json with' +
        'format {"publicKey":"PRIVATEKEYHERE","privateKey":"PUBLICKEYHERE"}');
    process.exit(1);
}
var publicKey = keys.publicKey;
var privateKey = keys.privateKey;


/**
 * Searches the marvel databases for character names that start with
 * the provided search text.
 * @param {string} searchText - The text the character names should start with
 * @param {int} pageStart - The position in the list of all character names with the searchText in them to begin pulling the names from
 * @param {int} pageCount - The number of characters to pull
 * @param {Func} cb - The function to callback on either error or success
 * @param {NodeCache} cache - Hashmap-type object that stores old requests
 */
function searchMarvel(searchText, pageStart, pageCount, searchSort, cb, cache) {

    //If it fails somewhere in here before even making the request, fine
    //just throw that up
    var ts = +new Date();
    var params = {
        ts: ts,
        apikey: publicKey,
        hash: md5(ts + privateKey + publicKey),
        nameStartsWith: searchText,
        orderBy: searchSort === 'asc' ? "name" : "-name",
        offset: pageStart,
        limit: pageCount
    };
    var url = baseUrl;
    Object.keys(params).forEach(function(key) {
        url += key + "=" + params[key] + "&";
    });
    url = url.substring(0, url.length - 1);

    var cacheUrl = "";
    var substrCacheUrls = [];
    var i = 0;
    for (i = 0; i < searchText.length; i += 1) {
        cacheUrl = "";
        Object.keys(params).forEach(function(key) {
            if (key != 'ts' && key != 'apikey' && key != 'hash') {
                var curVal = params[key];
                if (key == 'nameStartsWith' && i > 0) {
                    curVal = curVal.substring(0, i);
                }
                cacheUrl += key + "=" + curVal + "&";
            }
        });
        substrCacheUrls.push(cacheUrl);
    }

    var searchUrl = substrCacheUrls[0];
    var cachedRequest = cache.get(searchUrl);
    substrCacheUrls.some(function(subUrl) {
        var emptyRequest = cache.get(subUrl);
        if (emptyRequest && !emptyRequest.count) {
            cachedRequest = emptyRequest;
            return true;
        }
    });
    if (cachedRequest) {
        cb(cachedRequest.marvelNames, cachedRequest.count, 200);
        return;
    }

    //If it fails in here, handle that somehow
    request(url, function(error, resp, body) {
        var marvelNames = null;
        var count = 0;
        var statusCode = 500;
        //I'm ignoring error because resp should be null or statusCode should be non-200 if there is an error
        if (resp && resp.statusCode == 200) {
            try {
                var jsonBody = JSON.parse(body);
                marvelNames = [];
                count = jsonBody.data.total;
                var id = pageStart + 1;
                jsonBody.data.results.forEach(function(character) {
                    var link = "http://www.marvel.com";
                    if (character.urls) {
                        character.urls.forEach(function(curLink) {
                            if (curLink.type == "detail")
                                link = curLink.url;
                        });
                    }
                    marvelNames.push({
                        name: character.name,
                        id: id,
                        picture: character.thumbnail.path + '/' + 'standard_medium' + '.' + character.thumbnail.extension,
                        link: link
                    });
                    id += 1;
                });
                statusCode = 200;

                //I don't care if we throw an error setting the cache, I just don't
                //want it to throw up at all
                try {
                    cache.set(searchUrl, {
                        marvelNames: marvelNames,
                        count: count
                    });
                } catch (err) {
                    console.log("Cache error");
                    console.log(err);
                }
            } catch (err) {
                console.log("Response from marvel server was malformed");
                console.log(resp);
                console.log(err);
            }
        } else {
            console.log("Response from marvel server was not 200");
            console.log(error);
        }
        cb(marvelNames, count, statusCode);
    });
}

module.exports = searchMarvel;