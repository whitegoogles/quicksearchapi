/** 
 * index.js esponds to appropriately formed search requests with results 
 * from provided API modules
 **/


var express = require('express');
var app = express();
var NodeCache = require('node-cache');

//Sets up a cache that will invalidate results every 24 hours to 
//force a refresh
const requestsCache = new NodeCache({
    stdTTL: 24 * 60 * 60
});

//Global parameters
var SERVER_PORT = 3000;
var MAX_SEARCH_LENGTH = 100;

/*
 *Allows a user to include their own custom 
 *API modules as long as they fit a certain contract
 */
var searchModules = [
    require('./marvel.js')
];
var searchModule = searchModules[0];

//TODO Disable in prod as CORS is bad for security but necessary for dev
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/**
 * Searches using the current search module using the url parameters:
 * text = text to search for
 * start = offset in the complete of returned items that match the search
 * count = number of items to return 
 * sort = sort by ascending or descending alphabetically
 * timeStamp = UTF date when the client made the search request
 * @param {Object} req - The request as described above
 * @param {Object} res - The response to send with 
 *                      {count: number of items found in total,
                         results: JSON representation of the found items,
                         timeStamp: returns the passed client timestamp}
 */
app.get('/search', function(req, res) {
    console.log("Serving up a response at " + new Date());
    var query = req.query;
    var searchTime = 0;
    try {

        //I can't guarantee that the 3rd party APIs will handle poorly
        //formed data very well, so I need to sanity check these values
        var searchText = query.text.substring(0, 100).trim().toLowerCase();
        var pageStart = parseInt(query.start);
        var pageCount = parseInt(query.count);
        var searchSort = query.sort.toLowerCase() === 'asc' ? 'asc' : 'desc';
        searchTime = query.timeStamp;
        if (searchText && isFinite(pageStart) && isFinite(pageCount) &&
            pageStart >= 0 && pageCount > 0) {
            if (pageCount > 100) {
                pageCount = 100;
            }
            searchModule(searchText, pageStart, pageCount, searchSort, function(results, count, status) {
                res.status(status).json({
                    count: count,
                    results: results,
                    timeStamp: searchTime
                });
            }, requestsCache);
        } else {
            throw ("Parameters were not in a valid format " + JSON.stringify(req.query));
        }
    } catch (err) {
        console.log("Server threw error while parsing parameters or executing the custom search module");
        //console.log(err);
        res.status(500).json({
            timeStamp: searchTime
        });
    }
});

app.listen(3000);

module.exports = app;